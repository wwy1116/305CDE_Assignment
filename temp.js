
 /*
 var callback = function(err, data) {
    if(err) 
     return console.log(err)
    else 
     console.log(data);
 }
 
 var validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
 }
 
 var hash = function(password) {
  return crypto.createHash('sha1').update(password).digest('base64')
}

 var MemberSchema = new mongoose.Schema({
    username: String,
    password: String,
    email: String,
    updated_at: {type: Date, default: Date.now },
 });
 
 var Member = mongoose.model('Membership', MemberSchema);
 
 
 var CreateMember = function(username, password, email){
 if (!validateEmail(email)){
  console.log("Email incorrect.");
  return;
 }
 Member.find({username:username}, function(err, data) {
    if(err) 
     return console.log(err)
    else{ 
     if (data == ""){
      Member.create({
         username: username,
         password: hash(password),
         email: email
      }, callback);
      console.log(data);
      return 1;
     }else
      console.log("username is already existed!");
    }
 });
}
 
var VerifyLogin = function(username, password){
 // select by parameters
 Member.find({username:username, password:hash(password)}, callback);
}

var ChangePassword = function(username, password, newpassword){
 Member.update({username: username, password:hash(password)}, {password: hash(newpassword)}, {multi: false}, function
    (err, raw, numberAffected) {
    if(err) return console.error(err);
    console.log('The number of updated documents was %d', numberAffected);
    console.log('The raw response from Mongo was ', raw);
});
}

var RemoveMemberAll = function(username, volumeID) {
 Member.remove({}, callback);
}

 var https = require('https');
 var books = require('google-books-search');

var searchByName = function(name) {
 var url = 'https://www.googleapis.com/books/v1/volumes?q=' + name;
 https.get(url, function(res){
     var body = '';
 
     res.on('data', function(chunk){
         body += chunk;
     });
 
     res.on('end', function(){
         var fbResponse = JSON.parse(body);
         console.log("Got a response: ", fbResponse);
     });
 }).on('error', function(e){
       console.log("Got an error: ", e);
 });
}
 
 
 var searchByVolumeID = function(volumeID) {
  var url = 'https://www.googleapis.com/books/v1/volumes/' + volumeID;
  https.get(url, function(res){
      var body = '';
  
      res.on('data', function(chunk){
          body += chunk;
      });
  
      res.on('end', function(){
          var fbResponse = JSON.parse(body);
          console.log("Got a response: ", fbResponse);
      });
  }).on('error', function(e){
        console.log("Got an error: ", e);
  });
 }
 
 
 var searchByISBN = function(ISBN) {
  var url = 'https://www.googleapis.com/books/v1/volumes/p=isbn:' + ISBN;
  https.get(url, function(res){
      var body = '';
  
      res.on('data', function(chunk){
          body += chunk;
      });
  
      res.on('end', function(){
          var fbResponse = JSON.parse(body);
          console.log("Got a response: ", fbResponse);
      });
  }).on('error', function(e){
        console.log("Got an error: ", e);
  });
 }
 
 //CreateMember('abcd','123','abc@abc.com');
 //ChangePassword('abc','123','456');
 //VerfiyLogin('abc','123');
 //CreateFavourite('abc','123');
 //VerifyLogin('abc','1234');
 */