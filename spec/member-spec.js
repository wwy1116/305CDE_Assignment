var frisby = require('frisby')

/*  // globalSetup defines any settigs used for ALL requests */
frisby.globalSetup({
  request: {
    headers: {'Authorization': 'Basic dGVzdHVzZXI6cDQ1NXcwcmQ=','Content-Type': 'application/json'}
  }
})

frisby.create('add a new test')
  .get('http://localhost:8082/').toss();
  
/* in this second POST example we don't know precisely what values will be returned but we can check for the correct data types. Notice that the request body is passed as the second parameter and we need to pass a third parameter to indicate we are passing the data in json format. */
frisby.create('Add a new member "username": "abcde", "password": "123456", "email": "test2@test.com"')  //test add new member
  .post('http://localhost:8082/createMember', {"username": "abcde", "password": "123456", "email": "test2@test.com"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()
  
frisby.create('Change password member "username": "abcde", "password": "123456", "newpassword": "654321"')  //test change password
  .post('http://localhost:8082/changePassword', {"username": "abcde", "password": "123456", "newpassword": "654321"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()
  
  frisby.create('Verfiy login member "username": "abcde", "password": "654321"')  // test login
  .post('http://localhost:8082/verifyLogin', {"username": "abcde", "password": "654321"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  /*.expectJSONTypes({
    "status": Number,
    "message": String,
    "data": {
      "username": String,
      "password": String,
      "email": String
    }
  })*/
  .afterJSON( json => {
    // you can retrieve args using json.args.x
    expect(json.data.length).toEqual(1)
  })
  .toss()
  
  frisby.create('Remove all member')
  .get('http://localhost:8082/removeMemberAll', {}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()
