var frisby = require('frisby')

/*  // globalSetup defines any settigs used for ALL requests */
frisby.globalSetup({
  request: {
    headers: {'Authorization': 'Basic dGVzdHVzZXI6cDQ1NXcwcmQ=','Content-Type': 'application/json'}
  }
})

frisby.create('add a new test')
  .get('http://localhost:8082/').toss();

/* in this second POST example we don't know precisely what values will be returned but we can check for the correct data types. Notice that the request body is passed as the second parameter and we need to pass a third parameter to indicate we are passing the data in json format. */
frisby.create('Add a new favourite "username": "abcde", "volumeID": "guZHqGYwF5UC"')
  .post('http://localhost:8082/createFavourite', {"username": "abcde", "volumeID": "guZHqGYwF5UC"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()
  
frisby.create('Add notes to favourite "username": "abcde", "volumeID": "guZHqGYwF5UC", "notes": "Good book!"')
  .post('http://localhost:8082/addNoteToFavourite', {"username": "abcde", "volumeID": "guZHqGYwF5UC", "notes": "Good book!"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()
  
 frisby.create('List favourite "username": "abcde"')
  .post('http://localhost:8082/listFavourite', {"username": "abcde"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  /*.expectJSONTypes({
    "status": Number,
    "message": String,
    "data": {
      "username": String,
      "volumeID": String,
      "notes": String
    }
  })*/
  .toss()
  
 frisby.create('Remove a favourite "username": "abcde", "volumeID": "guZHqGYwF5UC"')
  .post('http://localhost:8082/removeFavourite', {"username": "abcde", "volumeID": "guZHqGYwF5UC"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()
  
 frisby.create('Remove all favourite')
  .get('http://localhost:8082/removeFavouriteAll', {}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()