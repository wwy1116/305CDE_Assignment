var frisby = require('frisby')

/*  // globalSetup defines any settigs used for ALL requests */
frisby.globalSetup({
  request: {
    headers: {'Authorization': 'Basic dGVzdHVzZXI6cDQ1NXcwcmQ=','Content-Type': 'application/json'}
  }
})

frisby.create('add a new test')
  .get('http://localhost:8082/').toss();

/* in this second POST example we don't know precisely what values will be returned but we can check for the correct data types. Notice that the request body is passed as the second parameter and we need to pass a third parameter to indicate we are passing the data in json format. */
frisby.create('Search book by name "bookname": "black cat"')
  .post('http://localhost:8082/searchBookByName', {"bookname": "black cat"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()
  
frisby.create('Search book by volume ID "volumeID": "guZHqGYwF5UC"')
  .post('http://localhost:8082/searchBookByVolumeID', {"volumeID": "guZHqGYwF5UC"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()
 
frisby.create('Search book by ISBN "ISBN": "9781471103902"')
  .post('http://localhost:8082/searchBookByISBN', {"ISBN": "9781471103902"}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()
  
frisby.create('Search free e-book')
  .post('http://localhost:8082/searchFreeEBook', {}, {json: true})
  .expectStatus(200)
  .inspectBody()
  .expectHeaderContains('content-type', 'application/json')
  .toss()