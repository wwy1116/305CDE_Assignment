var express = require('express');
var router = express.Router();
//var Member = require('../models/Member.js');
var crypto = require('crypto');
var mongoose = require('mongoose');

 var MemberSchema = new mongoose.Schema({
    username: String,
    password: String,
    email: String,
    updated_at: {type: Date, default: Date.now },
 });
 
 var Member = mongoose.model('Membership', MemberSchema);


 var callback = function(err, data) {
    if(err) 
     return console.log(err)
    else 
     console.log(data);
 }
 
 var validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
 }
 
 var hash = function(password) {
  return crypto.createHash('sha1').update(password).digest('base64')
}

router.get('/', function(req,res,next){
    console.log('Testing');
    res.end("test");
});

/*
router.post('/createMember', function(req,res){
 console.log("---Create Member---");
 var username = req.body.username;
 var password = req.body.password;
 var email = req.body.email;
 var msg = "create member successful";
 Member.find({username: username}, function(err, data) {
    if(err) 
     return console.log(err)
    else{ 
     if (data == ""){
      if (!validateEmail(email)){
       msg = "Email incorrect.";
      }else{
       Member.create({
          username: username,
          password: password,
          email: email
       }, function(err, data) {
           if(err) 
            return console.log(err)
           else 
            console.log(data);
        });
      }
     }else
      msg = "username is already existed!";
    }
    res.setHeader('content-type', 'application/json')
    //res.send(200, {status: 200, message: 'create member successful', data: data})
    res.status(res.statusCode).send({status: 'success', message: msg, data: data})
    res.end()
 });
});
*/

//create member
router.post('/createMember', function(req,res){
 console.log("---Create Member---");
 var username = req.body.username; //para from request
 var password = req.body.password;
 var email = req.body.email;
 console.log(username);
 var msg = "create member successful";
 var dataVal;
 Member.find({username: username}, function(err, data) { //mongodb find from member table
    if(err) 
     return console.log(err)
    else{ 
     if (Object.keys(data).length == 0){ //check member if exist
      // if (!validateEmail(email)){ //check email
      //  msg = "Email incorrect.";
      // }else{
       Member.create({
          username: username,
          password: password,
          email: email
       }, function(err, data2) {
           if(err) 
            return console.log(err)
           else{
            console.log(data2);
            dataVal = data2;
           }
        });
      // }
     }else
      msg = "username is already existed!"; 
    }
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('content-type', 'application/json')
    //res.send(200, {status: 200, message: 'create member successful', data: data})
    res.status(res.statusCode).send({status: 'success', message: msg, data: dataVal})
    res.end()
 });
});

//find member
router.post('/findMember', function(req,res){
 console.log("---Find Member---");
 var username = req.body.username;
 var msg = "find member successful";
 // select by parameters
 Member.find({username: username}, function(err, data) {
    if(err) 
     return console.log(err)
    else{ 
     if (data == ""){
      msg = "not exist";
     }else{
      console.log(data);
      msg = "exist";
     } 
    }
     res.setHeader('Access-Control-Allow-Origin', '*')
     res.setHeader('content-type', 'application/json')
     //res.send(200, {status: 200, message: 'verfiy login successful', data: data})
     res.status(res.statusCode).send({status: 'success', message: msg, data: data})
     res.end()
 });
});

//verify login
router.post('/verifyLogin', function(req,res){
 console.log("---Verify Login---");
 var username = req.body.username;
 var password = req.body.password;
 var msg = "verfiy login successful";
 // select by parameters
 Member.find({username: username, password: password}, function(err, data) {
    if(err) 
     return console.log(err)
    else{ 
     if (data == ""){
      msg = "failed";
     }else{
      console.log(data);
      msg = "success";
     } 
    }
     res.setHeader('content-type', 'application/json')
     //res.send(200, {status: 200, message: 'verfiy login successful', data: data})
     res.status(res.statusCode).send({status: 'success', message: msg, data: data})
     res.end()
 });
});

//change password
router.post('/changePassword', function(req,res){
 console.log("---Change Password---");
 var username = req.body.username;
 var password = req.body.password;
 var newpassword = req.body.newpassword;
 var msg = 'error';
 Member.update({username: username, password: password}, {password: newpassword}, {multi: false}, function
    (err, raw, numberAffected) {
    if(err) return console.error(err);
    else{
     console.log('The number of updated documents was %d', numberAffected);
     console.log('The raw response from Mongo was ', raw);
     if (raw.nModified == 1) msg = 'success';
    }
    
    
    res.setHeader('content-type', 'application/json')
     //res.send(200, {status: 200, message: 'change password successful'})
     res.status(res.statusCode).send({status: 'success', message: msg})
     res.end()
 });
     
});

//remove all member
router.get('/removeMemberAll', function(req,res){
 console.log("---Remove All Member---");
 Member.remove({}, function(err, data) {
    if(err) 
     return console.log(err)
    else{ 
     console.log(data);
    }
  res.setHeader('content-type', 'application/json')
  //res.send(200, {status: 200, message: 'remove all member successful', data: data})
  res.status(res.statusCode).send({status: 'success', message: 'remove all member successful', data: data})
  res.end()
 });
});

/*
exports.CreateMember = CreateMember;
exports.VerifyLogin = VerifyLogin;
exports.ChangePassword = ChangePassword;
exports.RemoveMemberAll = RemoveMemberAll;
*/

module.exports = router;
