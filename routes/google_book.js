var express = require('express');
var router = express.Router();
var https = require('https');
var books = require('google-books-search');

router.get('/', function(req,res,next){
    console.log('Testing');
});

//search book by name
router.post('/searchBookByName', function(req,res){
 console.log("---Search Book By Name---");
 var bookname = req.body.bookname;
 var url = 'https://www.googleapis.com/books/v1/volumes?q=' + bookname + "&filter=free-ebooks&maxResults=40";
 var data;
 https.get(url, function(res2){
     var body = '';
 
     res2.on('data', function(chunk){
         body += chunk;
     });
 
     res2.on('end', function(){
         var fbResponse = JSON.parse(body);
         //console.log("Got a response: ", fbResponse);
         res.setHeader('content-type', 'application/json')
         //res.send(200, {status: 200, message: 'search book by name successful'})
         res.status(res.statusCode).send({status: 'success', message: 'search book by name successful', data: fbResponse})
         res.end()
     });
     
     
 }).on('error', function(e){
       console.log("Got an error: ", e);
       return;
 });
 
});
 
//search book by volume id
router.post('/searchBookByVolumeID', function(req,res){
 console.log("---Search Book By Volume ID---");
 var volumeID = req.body.volumeID;
  var url = 'https://www.googleapis.com/books/v1/volumes/' + volumeID;
  https.get(url, function(res2){
      var body = '';
  
      res2.on('data', function(chunk){
          body += chunk;
      });
  
      res2.on('end', function(){
          var fbResponse = JSON.parse(body);
          console.log("Got a response: ", fbResponse);
          res.setHeader('content-type', 'application/json')
          //res.send(200, {status: 200, message: 'search book by volume ID successful'})
          res.status(res.statusCode).send({status: 'success', message: 'search book by volume ID successful', data: fbResponse})
          res.end()
      });
      
  }).on('error', function(e){
        console.log("Got an error: ", e);
        return;
  });
  
 });

//search book by isnbn
router.post('/searchBookByISBN', function(req,res){
 console.log("---Search Book By ISBN---");
 var ISBN = req.body.ISBN;
 var url = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' + ISBN;
  https.get(url, function(res){
      var body = '';
  
      res.on('data', function(chunk){
          body += chunk;
      });
  
      res.on('end', function(){
          var fbResponse = JSON.parse(body);
          //console.log("Got a response: ", fbResponse);
      });
      
  }).on('error', function(e){
        console.log("Got an error: ", e);
  });
  res.setHeader('content-type', 'application/json')
  //res.send(200, {status: 200, message: 'search book by ISBN successful'})
  res.status(res.statusCode).send({status: 'success', message: 'search book by ISBN successful'})
  res.end()
 });
 
//search free e-book
 router.post('/searchFreeEBook', function(req,res){
 console.log("---Search Free E-Book---");
 var ISBN = req.body.ISBN;
 var url = 'https://www.googleapis.com/books/v1/volumes?q=filter=free-ebooks&maxResults=40';
  https.get(url, function(res){
      var body = '';
  
      res.on('data', function(chunk){
          body += chunk;
      });
  
      res.on('end', function(){
          var fbResponse = JSON.parse(body);
          //console.log("Got a response: ", fbResponse);
      });
      
  }).on('error', function(e){
        console.log("Got an error: ", e);
  });
  res.setHeader('content-type', 'application/json')
  //res.send(200, {status: 200, message: 'search book by ISBN successful'})
  res.status(res.statusCode).send({status: 'success', message: 'search free e-book successful'})
  res.end()
 });
 
 module.exports = router;