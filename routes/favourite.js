var express = require('express');
var router = express.Router();
//var Favourite = require('../models/Favourite.js');
var crypto = require('crypto');
var mongoose = require('mongoose');

 var FavouriteListSchema = new mongoose.Schema({
    username: String,
    volumeID: String,
    notes: String,
    updated_at: {type: Date, default: Date.now },
 });
 
 var FavouriteList = mongoose.model('FavouriteList', FavouriteListSchema);
 
 var callback = function(err, data) {
    if(err) 
     return console.log(err)
    else
     console.log(data);
 }
 
router.get('/', function(req,res,next){
    console.log('Testing');
});

//create favourite
router.post('/createFavourite', function(req,res){
 console.log("---Create Favourite---");
 var username = req.body.username;
 var volumeID = req.body.volumeID;
 var dataVal;
 var msg;
 FavouriteList.find({username: username, volumeID: volumeID}, function(err, data) { //mongodb find from favourite table
    if(err) 
     return console.log(err)
    else{ 
     if (Object.keys(data).length == 0){ //check favourite if exist
       FavouriteList.create({
          username: username,
          volumeID: volumeID,
          notes: ''
       }, function(err, data2) {
           if(err) 
            return console.log(err)
           else{
            console.log(data2);
            msg='success'
            dataVal = data2;
            
           }
        });
      // }
     }else
      msg = "exist"; 
    }
     res.setHeader('Access-Control-Allow-Origin', '*')
     res.setHeader('content-type', 'application/json')
     //res.send(200, {status: 200, message: 'create favourite successful', data: data})
     res.status(res.statusCode).send({status: 'success', message: msg, data: dataVal})
     res.end()
 });
});

/*
exports.createFavourite = function(username, volumeID) {
 console.log("---Create Favourite---");
 var username = username;
 var volumeID = volumeID;
 FavouriteList.create({
    username: username,
    volumeID: volumeID
 }, function(err, data) {
    if(err) 
     return console.log(err)
    else{ 
     console.log(data);
     return {code:200, response:{status:'success', contentType:'application/json', message:'list found', data: data}}
    }
 });
}
*/

//remove favourite
router.post('/removeFavourite', function(req,res){
 console.log("---Remove Favourite---");
 var username = req.body.username;
 var volumeID = req.body.volumeID;
 FavouriteList.remove({
    username: username,
    volumeID: volumeID,
 }, function(err, data) {
    if(err) 
     return console.log(err)
    else
     console.log(data);
    
     res.setHeader('content-type', 'application/json')
     //res.send(200, {status: 200, message: 'remove favourite successful', data: data})
     res.status(res.statusCode).send({status: 'success', message: 'remove favourite successful', data: data})
     res.end()
 });
});

//remove all favourite
router.get('/removeFavouriteAll', function(req,res){
 console.log("---Remove All Favourite---");
 FavouriteList.remove({}, function(err, data) {
    if(err) 
     return console.log(err)
    else
     console.log(data);
     
     res.setHeader('content-type', 'application/json')
     //res.send(200, {status: 200, message: 'remove all favourite successful', data: data})
     res.status(res.statusCode).send({status: 'success', message: 'remove all favourite successful', data: data})
     res.end()
 });
});

//list favourite
router.post('/listFavourite', function(req,res){
 console.log("---List Favourite---");
 var username = req.body.username;
 FavouriteList.find({username: username}, function(err, data) {
    if(err) 
     return console.log(err)
    else
     console.log(data);
    
    res.setHeader('content-type', 'application/json')
    //res.send(200, {status: 200, message: 'list favourite successful', data: data})
    res.status(res.statusCode).send({status: 'success', message: 'list favourite successful', data: data})
    res.end()
 });
});

//add note to favourite
router.post('/addNoteToFavourite', function(req,res){
 console.log("---Add Note To Favourite---");
 var username = req.body.username;
 var volumeID = req.body.volumeID;
 var notes = req.body.notes;
 FavouriteList.update({username: username, volumeID: volumeID}, {notes: notes}, {multi: false}, function
    (err, raw, numberAffected) {
        if(err) return console.error(err);
        else{
         console.log('The number of updated documents was %d', numberAffected);
         console.log('The raw response from Mongo was ', raw);
        }
        res.setHeader('content-type', 'application/json')
        //res.send(200, {status: 200, message: 'add note to favourite successful'})
        res.status(res.statusCode).send({status: 'success', message: 'add note to favourite successful'})
        res.end()
       
 });
});

/*
exports.CreateFavourite = CreateFavourite;
exports.RemoveFavourite = RemoveFavourite;
exports.ListFavourite = ListFavourite;
*/
    
module.exports = router;
