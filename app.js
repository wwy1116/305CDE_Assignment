 var express = require('express');
 var path = require('path');
 var favicon = require('serve-favicon');
 var logger = require('morgan');
 var cookieParser = require('cookie-parser');
 var bodyParser = require('body-parser');
 var crypto = require('crypto')
 
 //var routes = require('./routes/index');
 //var todos = require('./routes/todos');
 var member = require('./routes/member');
 var favourite = require('./routes/favourite');
 var google_book = require('./routes/google_book');
 
 var index = require('./routes/index');
 
 var mongoose = require('mongoose');
 mongoose.connect('mongodb://collection:collection@ds023664.mlab.com:23664/sampledb305cde', function(err) {
     if(err) {
         console.log('connection error', err);
     } else {
         console.log('connection successful');
     }
 });
 
 var app = express();
 
 // view engine setup
 app.set('views', path.join(__dirname, 'views'));
 app.set('view engine', 'ejs');
 
 app.use('/css', express.static('css'));
 
 // uncomment after placing your favicon in /public
 //app.use(favicon(__dirname + '/public/favicon.ico'));
 app.use(logger('dev'));
 app.use(bodyParser.json());
 app.use(bodyParser.urlencoded({ extended: false }));
 app.use(cookieParser());
 app.use(express.static(path.join(__dirname, 'public')));
 
 app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
 });

 
 //app.use('/', routes);
 //app.use('/todo', todos);
 app.use('/', member);
 //app.use('/test', member);
 app.use('/createMember', member);
 app.use('/findMember', member);
 app.use('/verifyLogin', member);
 app.use('/changePassword', member);
 app.use('/removeMemberAll', member);
 
 app.use('/', favourite);
 app.use('/createFavourite', favourite);
 app.use('/listFavourite', favourite);
 app.use('/addNoteToFavourite', favourite)
 app.use('/removeFavourite', favourite);
 app.use('/removeFavouriteAll', favourite);

 app.use('/', google_book);
 app.use('/searchBookByName', google_book);
 app.use('/searchBookByVolumeID', google_book);
 app.use('/searchBookByISBN', google_book);
 app.use('/searchFreeEBook', google_book);
 
 app.get('/registration', function(req,res){
     res.sendFile(path.join(__dirname + '/views/registration.html'));
 });
 app.get('/changePasswordPage', function(req,res){
     res.sendFile(path.join(__dirname + '/views/changePassword.html'));
 });
 app.get('/favouriteList', function(req,res){
     res.sendFile(path.join(__dirname + '/views/favouriteList.html'));
 });
 app.get('/login', function(req,res){
     res.sendFile(path.join(__dirname + '/views/login.html'));
 });
 app.get('/index', function(req,res){
     res.sendFile(path.join(__dirname + '/views/index.html'));
 });
 
 
 
 // catch 404 and forward to error handler
 app.use(function(req, res, next) {
     var err = new Error('Not Found');
     err.status = 404;
     next(err);
 });
 
 // error handlers
 
 // development error handler
 // will print stacktrace
 if (app.get('env') === 'development') {
     app.use(function(err, req, res, next) {
         res.status(err.status || 500);
         res.render('error', {
             message: err.message,
             error: err
         });
     });
 }
 
 
 // production error handler
 // no stacktraces leaked to user
 app.use(function(err, req, res, next) {
     res.status(err.status || 500);
     res.render('error', {
         message: err.message,
         error: {}
     });
 });
 
 
//var port = process.env.PORT || 8080;
var port = 8082 || 8080;
app.listen(port, function (err) {
  if (err) {
      console.error(err);
  } else {
    console.log('App is ready at : ' + port);
  }
})
 
exports.closeServer = function(){
  app.close();
};
 
 module.exports = app;
 